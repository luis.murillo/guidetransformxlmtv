﻿using com.eninetworks.utilerias.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eninetworks.enitv.horarios
{
    class ArchivosContenidos
    {
        public int Indice { get; }
        public string Tipo { get; }
        public TipoProcesarArchivo TipoProcesarArchivo { get; }
        public List<string> Permitidos { get; }
        public ArchivosContenidos(TipoProcesarArchivo tipoProcesarArchivo, string tipo)
        {
            TipoProcesarArchivo = tipoProcesarArchivo;
            Tipo = tipo;
            Indice = GetIndiceByName();
            Permitidos = new List<string>( new string[] {
                    "sheet" + Indice + ".xml",
                    "drawing" + Indice + ".xml",
                    "sharedstrings.xml"
                });
        }
        private int GetIndiceByName()
        {
            string valor = Tipo.Split('.')[0];
            Int32.TryParse(valor.Substring(valor.Length - 2, 2), out int resultado);
            if (resultado == 0)
                Int32.TryParse(valor.Substring(valor.Length - 1, 1), out resultado);
            return resultado;
        }
        public string GetName(string dato) => dato.Split('/')[dato.Split('/').Length - 1];
    }
}
