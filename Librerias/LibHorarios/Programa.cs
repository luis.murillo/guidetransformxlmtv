﻿using com.eninetworks.utilerias.comun;
using com.eninetworks.utilerias.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class Programa
    {
        private readonly string mensaje;
        public Dias Dias { get; }
        public SharedString Programacion { get; }
        public double HoraInicio { get; }
        public double MinutoInicio { get; }
        public int Duracion { get; }
        public DateTime FechaInicio { get; }
        public DateTime FechaFin { get; }
        public Programa(TipoRow row,Control control)
        {
            mensaje = "";
            Dias = new Dias();
            FechaInicio = control.Limites.GetFechaInicio(control,row);
            FechaFin = control.Limites.GetFechaFin(control, row);
            HoraInicio = control.Limites.GetHoraInicio(control, row);
            MinutoInicio = control.Limites.GetMinutoInicio(control, row);
            Duracion = Convert.ToInt32((FechaFin - FechaInicio).TotalMinutes);
            Programacion = new SharedString(row, control);
            Dias.Items.Add(new DiaSemana(row.Indice,control, FechaInicio));
        }
        public Programa(TipoCel celda, Control control ,int hora,int minuto)
        {
            mensaje = "";
            try
            {
                Dias = new Dias();
                MergeCell mc = MergeCells.GetMergeCell(celda.Indice);
                Duracion = control.Limites.DuracionRow;
                if(!(mc is null))
                Duracion = mc.Duracion * control.Limites.DuracionRow;
                HoraInicio = hora;
                MinutoInicio = minuto;
                Programacion = control.SharedStrings.GetItem(celda.Value);
                Dias.Items.AddRange(DiasSemanas.GetDiasSemanas(mc,celda));
            }catch(Exception e)
            {
                mensaje = e.Message;
            }
        }
        public string Mensaje() => mensaje;
        public void SetNodo(XmlElement element)
        {
            XmlElement programa = Utilerias.Documento.CreateElement(string.Empty, "Programa", string.Empty);
            programa.AppendChild(Utilerias.SetTextNode("HoraInicio", HoraInicio.ToString()));
            programa.AppendChild(Utilerias.SetTextNode("MinutoInicio", MinutoInicio.ToString()));
            programa.AppendChild(Utilerias.SetTextNode("Duracion", Duracion.ToString()));
            programa.AppendChild(Dias.GetNodo());
            programa.AppendChild(Programacion.GetNodo());
            element.AppendChild(programa);
        }
        public void SetNodoTV2(XmlElement element, string canal)
        {
            XmlElement programa = Utilerias.Documento.CreateElement(string.Empty, "programme", string.Empty);
            DateTime fecha = FechaInicio;
            programa.SetAttribute("start", fecha.ToString("yyyyMMddHHmmss") + " " + Propiedades.GetStringPropertie("DIFERENCIA"));
            programa.SetAttribute("stop", fecha.AddMinutes(Duracion).ToString("yyyyMMddHHmmss") + " " + Propiedades.GetStringPropertie("DIFERENCIA"));
            programa.SetAttribute("channel", canal);
            XmlElement title = Utilerias.SetTextNode("title", Programacion.Programa);
            title.SetAttribute("lang", "es");
            programa.AppendChild(title);
            XmlElement desc = Utilerias.SetTextNode("desc", Programacion.Detalle);
            desc.SetAttribute("lang", "es");
            programa.AppendChild(desc);
            programa.AppendChild(Utilerias.SetTextNode("date", fecha.ToString("yyyyMMdd")));
            XmlElement categoria = Utilerias.SetTextNode("category", "Newsmagazine");
            categoria.SetAttribute("lang", "en");
            programa.AppendChild(categoria);
            XmlElement episode = Utilerias.SetTextNode("episode-num", "EP01006886.0028");
            episode.SetAttribute("system", "dd_progid");
            programa.AppendChild(episode);
            XmlElement episode2 = Utilerias.SetTextNode("episode-num", "427");
            episode2.SetAttribute("system", "onscreen");
            programa.AppendChild(episode2);
            XmlElement audio = Utilerias.SetTextNode("audio", "");
            audio.AppendChild(Utilerias.SetTextNode("stereo", "stereo"));
            programa.AppendChild(audio);
            programa.AppendChild(Utilerias.Documento.CreateElement(string.Empty, "previously-shown", string.Empty));
            XmlElement subtitles = Utilerias.Documento.CreateElement(string.Empty, "subtitles", string.Empty);
            subtitles.SetAttribute("type", "teletext");
            programa.AppendChild(subtitles);
            element.AppendChild(programa);
        }
        public void SetNodoTV(XmlElement element, string canal)
        {
            foreach (DiaSemana item in Dias.Items)
            {
                XmlElement programa = Utilerias.Documento.CreateElement(string.Empty, "programme", string.Empty);
                DateTime fecha = item.Fecha;
                programa.SetAttribute("start", fecha.AddHours(HoraInicio).AddMinutes(MinutoInicio).ToString("yyyyMMddHHmmss") + " " + Propiedades.GetStringPropertie("DIFERENCIA"));
                programa.SetAttribute("stop", fecha.AddHours(HoraInicio).AddMinutes(MinutoInicio + Duracion).ToString("yyyyMMddHHmmss") + " " + Propiedades.GetStringPropertie("DIFERENCIA"));
                programa.SetAttribute("channel", canal);
                XmlElement title = Utilerias.SetTextNode("title", Programacion.Programa);
                title.SetAttribute("lang", "es");
                programa.AppendChild(title);
                XmlElement desc = Utilerias.SetTextNode("desc", Programacion.Detalle);
                desc.SetAttribute("lang", "es");
                programa.AppendChild(desc);
                programa.AppendChild(Utilerias.SetTextNode("date", fecha.ToString("yyyyMMdd")));
                XmlElement categoria = Utilerias.SetTextNode("category", "Newsmagazine");
                categoria.SetAttribute("lang", "en");
                programa.AppendChild(categoria);
                XmlElement episode = Utilerias.SetTextNode("episode-num", "EP01006886.0028");
                episode.SetAttribute("system", "dd_progid");
                programa.AppendChild(episode);
                XmlElement episode2 = Utilerias.SetTextNode("episode-num", "427");
                episode2.SetAttribute("system", "onscreen");
                programa.AppendChild(episode2);
                XmlElement audio = Utilerias.SetTextNode("audio", "");
                audio.AppendChild(Utilerias.SetTextNode("stereo", "stereo"));
                programa.AppendChild(audio);
                programa.AppendChild(Utilerias.Documento.CreateElement(string.Empty, "previously-shown", string.Empty));
                XmlElement subtitles = Utilerias.Documento.CreateElement(string.Empty, "subtitles", string.Empty);
                subtitles.SetAttribute("type", "teletext");
                programa.AppendChild(subtitles);
                element.AppendChild(programa);
            }
        }
    }
}

