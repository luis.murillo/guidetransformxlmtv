﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class TipoRow
    {
        public int Indice { get; set; }
        public string Spans { get; set; }
        public decimal HT { get; set; }
        public int CustonHeight { get; set; }
        public int ThickBot { get; set; }
        public List<TipoCel> Items { get; set; }
        public TipoRow(XmlNode node)
        {
            try
            {

                Items = new List<TipoCel>();
                Indice = Convert.ToInt32(node.Attributes["r"].Value);
                Spans = node.Attributes["spans"].Value;
                HT = Convert.ToDecimal(Utilerias.GetValor(node, "ht"));
                ThickBot = Convert.ToInt32(node.Attributes["r"].Value);
                CustonHeight = Convert.ToInt32(Utilerias.GetValor(node, "thickBot"));
                foreach (XmlNode item in node.ChildNodes)
                    Items.Add(new TipoCel(item, Indice));
            }
            catch (Exception ex) 
            { 
            }
        }
    }
}
