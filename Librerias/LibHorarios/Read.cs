﻿using com.eninetworks.enitv.horarios;
using com.eninetworks.utilerias.comun;
using com.eninetworks.utilerias.enums;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.OpenXml4Net.OPC;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace com.eninetworks.utilerias.excel
{
    class Read
    {
        public static Extension Extension;
        public TipoProcesarArchivo TipoProcesarArchivo { get; }
        public Files Archivos {get; set;}
        protected List<string> DateSoport { get; set; }
        protected HSSFWorkbook wbxls;
        protected HSSFSheet shxls;
        protected XSSFWorkbook wbxlsx;
        protected XSSFSheet shxlsx;
        protected List<string> Lineas { get; set; }
        public List<string> Hojas { get; set; }
        public List<Row> Rows { get; set; }
        protected Row Encabezado { get; set; }
        public string File { get; set; }
        public string Hoja { get; set; }
        public int IndiceInicio { get; }
        public bool SaveFiles { get; }
        public Read(string file, TipoProcesarArchivo tipoProcesarArchivo, int indice =0)
        {
            TipoProcesarArchivo = tipoProcesarArchivo;
            File = file;
            IndiceInicio = indice;
            SaveFiles = Propiedades.GetBoolPropertie("SALVARFILES");
            DateSoport = new List<string>("14,15,20,21,180,181,182,183,184,185,186".Split(','));
            Extension = Extension.NONE;
            Hojas = new List<string>();
            Rows = new List<Row>();
            SetExtension();
            LoadHojas();
        }
        private void SetExtension()
        {
            Extension = Extension.NONE;
            string extension = Path.GetExtension(File).ToUpper().Replace(".", "");
            if (extension == "XLS")
                Extension = Extension.XLS;
            else if (extension == "XLSX")
                Extension = Extension.XLSX;
            else if (extension == "CSV")
                Extension = Extension.CSV;
        }
        public void SetFirstSheet()
        {
            if (Hojas.Count > 0)
                Hoja = Hojas[0];
            else
                Hoja = "";
        }
        private void LoadHojas()
        {
            if (Extension.Equals(Extension.XLS))
            {
                using (var fs = new FileStream(File, FileMode.Open, FileAccess.Read))
                {
                    wbxls = new HSSFWorkbook(fs);
                    for (int i = 0; i < wbxls.Count; i++)
                        Hojas.Add(wbxls.GetSheetAt(i).SheetName);
                }
            }
            else if (Extension.Equals(Extension.XLSX))
            {
                using (var fs = new FileStream(File, FileMode.Open, FileAccess.Read))
                {
                    wbxlsx = new XSSFWorkbook(fs);
                    for (int i = 0; i < wbxlsx.Count; i++)
                        Hojas.Add(wbxlsx.GetSheetAt(i).SheetName);
                }
            }
            else if (Extension.Equals(Extension.CSV))
            {
                Hojas.Add("Default");
            }

        }
        public void GetCSV()
        {
            Lineas = new List<string>();
            using (var reader = new StreamReader(File, Encoding.UTF8))
            {
                while (!reader.EndOfStream)
                {
                    Lineas.Add(reader.ReadLine());
                }
            }
        }
        public void Load(string hoja,bool getrows = false)
        {
            if (Extension.Equals(Extension.XLS))
                shxls = (HSSFSheet)wbxls.GetSheet(hoja);
            else if (Extension.Equals(Extension.XLSX))
                shxlsx = (XSSFSheet)wbxlsx.GetSheet(hoja);
            else if (Extension.Equals(Extension.CSV))
                GetCSV();
            if (!getrows)
                SetFiles();
            else
                GetRows();
        }
        private void GetRows()
        {
            Encabezado = new Row();
            Rows = new List<Row>();
            if (Extension.Equals(Extension.XLS))
            {
                if (shxls.PhysicalNumberOfRows > 0)
                    Encabezado = SetRow(shxls.GetRow(IndiceInicio));
                for (int i = IndiceInicio + 1; i < shxls.PhysicalNumberOfRows; i++)
                    Rows.Add(SetRow(shxls.GetRow(i)));
            }
            else if (Extension.Equals(Extension.XLSX))
            {
                if (shxlsx.PhysicalNumberOfRows > 0)
                    Encabezado = SetEncabezado(shxlsx.GetRow(IndiceInicio));
                for (int i = IndiceInicio + 1; i < shxlsx.PhysicalNumberOfRows; i++)
                    Rows.Add(SetRow(shxlsx.GetRow(i)));
            }
            else if (Extension.Equals(Extension.CSV))
            {
                if (Lineas.Count > 0)
                {
                    Encabezado = SetRow(Lineas[0].Split(','));
                    Lineas.RemoveAt(0);
                }
                foreach (string item in Lineas)
                {
                    Rows.Add(SetRow(item.Split(',')));
                }
            }
        }
        private Row SetEncabezado(IRow irow)
        {
            Row row = new Row();
            if (irow != null)
            {
                for (int i = 0; i < irow.LastCellNum; i++)
                    row.Cells.Add(irow.GetCell(i) == null ? "" : irow.GetCell(i).ToString());
            }
            return row;
        }
        private Row SetRow(string[] irow)
        {
            Row row = new Row();
            foreach (string item in irow)
                row.Cells.Add(item.ToString());
            return row;
        }
        private Row SetRow(IRow irow)
        {
            Row row = new Row();
            if (irow != null)
                for (int i = 0; i < Encabezado.Cells.Count; i++)
                    row.Cells.Add(SetStyle(irow.GetCell(i)));
            for (int i = row.Cells.Count; i < Encabezado.Cells.Count; i++)
                row.Cells.Add("");

            return row;
        }
        private void SetFiles()
        {
            Rows = new List<Row>();
            Encabezado = new Row();
            Archivos = new Files();
            if (Extension.Equals(Extension.XLS))
            {
                POIXMLDocument df = ((NPOI.POIXMLDocument)shxls.Workbook);
                ArchivosContenidos archivosContenidos = new ArchivosContenidos(TipoProcesarArchivo, shxls.ToString());
                foreach (PackagePart item in df.Package.GetParts())
                    if (archivosContenidos.Permitidos.Find(e => item.PartName.Name.ToLower().EndsWith(e)) != null)
                        AddFile(item, archivosContenidos);
            }
            else if (Extension.Equals(Extension.XLSX))
            {
                POIXMLDocument df = ((NPOI.POIXMLDocument)shxlsx.Workbook);
                ArchivosContenidos archivosContenidos = new ArchivosContenidos(TipoProcesarArchivo,shxlsx.ToString());
                foreach (PackagePart item in df.Package.GetParts())
                    if (archivosContenidos.Permitidos.Find(e => item.PartName.Name.ToLower().EndsWith(e)) != null)
                        AddFile(item, archivosContenidos);
            }
        }
        private void AddFile(PackagePart item, ArchivosContenidos archivosContenidos)
        {
            Archivos.Add(item.GetStream(FileMode.Open), archivosContenidos.GetName(item.PartName.Name));
            if (SaveFiles)
                using (var fileStream = new FileStream(Path.Combine(Propiedades.GetStringPropertie("PATHSAVE"), Archivos.GetItem().FileName), FileMode.Create, FileAccess.Write))
                {
                    item.GetStream(FileMode.Open).CopyTo(fileStream);
                }
        }
        public DataTable GetDataTable()
        {
            DataTable datatable = new DataTable();
            foreach (string cell in Encabezado.Cells)
            {
                try
                {
                    datatable.Columns.Add(cell);
                }
                catch (Exception)
                {
                    datatable.Columns.Add(cell + datatable.Columns.Count);
                }

            }
            foreach (Row row in Rows)
            {
                string[] array = new string[row.Cells.Count];
                int count = 0;
                foreach (string cell in row.Cells)
                {
                    array[count] = cell;
                    count++;
                }
                datatable.Rows.Add(array);
            }
            return datatable;
        }
        private string SetStyle(ICell cell)
        {
            try
            {
                if (cell == null)
                    return "";
                if (DateSoport.Find(e => e == cell.CellStyle.DataFormat.ToString() ? true : false) != null)
                {
                    if (cell.CellType == CellType.Numeric)
                        return cell.DateCellValue.ToString("dd/MM/yyyy HH:mm");
                    else
                        return cell.StringCellValue;
                }
                else
                {
                    if (cell.CellType == CellType.Numeric)
                        return cell.NumericCellValue.ToString();
                    else if (cell.CellType == CellType.Blank)
                        return "";
                    else
                        return cell.StringCellValue;
                }
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
