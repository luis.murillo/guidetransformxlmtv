﻿using com.eninetworks.utilerias.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eninetworks.enitv.horarios
{
    static class DiasSemanas
    {
        public static List<DiaSemana> Items { get; set; }
        public static int Inicio { get; set; }
        private static int Selected { get; set; }
        public static void Init() { Items = new List<DiaSemana>(); }
        public static void Load(TipoRow dia, TipoRow numero, Control Control)
        {
            try
            {
                for (int i = Control.Limites.GetInicioCelda(); i < Control.Limites.GetFinCelda(); i++)
                    if (Control.Limites.Programas.Contains(dia.Items[i].Indice.Substring(0, 1)))
                        if (Control.Limites.GetDia() == Control.Limites.GetNumeroDia())
                            Items.Add(new DiaSemana(numero.Items[i], Items.Count, Control));
                        else
                            Items.Add(new DiaSemana(dia.Items[i], numero.Items[i], Items.Count, Control));
            }
            catch (Exception ex)
            {
            }
        }
        public static DiaSemana GetDiaSemana(string letra)
        {
            return Items.Find(e => e.Letra == letra.ToUpper());
        }
        public static List<DiaSemana> GetDiasSemanas(MergeCell mc,TipoCel celda)
        {
            List<DiaSemana> lista = new List<DiaSemana>();
            if (!(mc is null))
            {
                Add(lista, GetDiaSemana(mc.Inicio.Letra));
                while (!lista[lista.Count - 1].Letra.Contains(mc.Fin.Letra))
                {
                    Next();
                    Add(lista, Items[Selected]);
                }
            }
            else
            {
                Add(lista, GetDiaSemana(celda.Indice.Substring(0,1)));
            }
            return lista;
        }
        private static void Add(List<DiaSemana> lista, DiaSemana item)
        {
            lista.Add(item);
            Selected = lista[lista.Count - 1].Indice;
        }
        private static int Next()
        {
            if (Selected < Items.Count)
                Selected += 1;
            return Selected;
        }
        public static void Clear()
        {
            Items?.Clear();
        }
        public static bool IsProgram(string indice,bool sharedString)
        {
            if (Convert.ToInt32(indice.ToUpper().Substring(1, indice.Length - 1)) <= Inicio)
                return false;
            if (sharedString && Items.FindAll(e => e.Letra==(indice.ToUpper().Substring(0, 1))).Count>0)
                return true;
            else
                return false;
        }
    }
}
