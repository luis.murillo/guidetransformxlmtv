﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    static class Utilerias
    {
        public static XmlDocument Documento { get; set; }
        public static XmlElement Root { get; set; }

        public static string GetValor(XmlNode node, string dato)
        {
            string valor = "0";
            try
            {
                valor = node.Attributes[dato].Value;
            }
            catch (Exception)
            {

            }
            return valor;
        }
        public static int GetDiasSemana(int inicio,string columna)
        {
            string valor = "ABCDEFGHIJKLMNOPQRSTVWXYZ";
            int indice = inicio-1;
            int dia = 0;
            bool seguir = true;
            do
            {
                indice++;
                dia++;
                if (valor.Substring(indice, 1).Contains(columna))
                    seguir = false;
            } while (seguir);
            return dia;
        }
        public static string GetLetraCelda(string dato)
        {
            string valor = "1234567890";
            int indice = -1;
            bool seguir = true;
            do
            {
                indice++;
                if (valor.Contains(dato.Substring(indice, 1)))
                    seguir = false;
            } while (seguir);
            return dato.Substring(0, indice);
        }
        public static string GetDiaSemana(double numero)
        {
            string dia = "";
            if (numero == 1) dia = "LUNES";
            else if (numero == 2) dia = "MARTES";
            else if (numero == 3) dia = "MIERCOLES";
            else if (numero == 4) dia = "JUEVES";
            else if (numero == 5) dia = "VIERNES";
            else if (numero == 6) dia = "SABADO";
            else if (numero == 7) dia = "DOMINGO";
            return dia;
        }
        public static string GetLetraSemana(double numero)
        {
            string dia = "";
            if (numero == 1) dia = "C";
            else if (numero == 2) dia = "D";
            else if (numero == 3) dia = "E";
            else if (numero == 4) dia = "F";
            else if (numero == 5) dia = "G";
            else if (numero == 6) dia = "H";
            else if (numero == 7) dia = "I";
            return dia;
        }
        public static void CreateXMLDocument()
        {
            Documento =  new XmlDocument();
            XmlDeclaration xmlDeclaration = Documento.CreateXmlDeclaration("1.0", "UTF-8", null);
            Documento.AppendChild(Utilerias.Documento.CreateElement(string.Empty, "ENI", string.Empty));
            Root = Documento.DocumentElement;
            Documento.InsertBefore(xmlDeclaration, Root);

        }
        public static void CreateXMLDocumentTV()
        {
            Documento = new XmlDocument();
            XmlDeclaration xmlDeclaration = Documento.CreateXmlDeclaration("1.0", "UTF-8", null);
            //XmlDocumentType xmlDocumentType = Documento.CreateDocumentType("tv",null, "xmltv.dtd", null);
            Documento.AppendChild(Utilerias.Documento.CreateElement(string.Empty, "tv", string.Empty));
            Root = Documento.DocumentElement;
            Documento.InsertBefore(xmlDeclaration, Root);
            //Documento.InsertBefore(xmlDocumentType, Root);

        }
        public static XmlElement SetTextNode(string name, string value)
        {
            XmlElement elemento = Utilerias.Documento.CreateElement(string.Empty, name, string.Empty);
            elemento.AppendChild(Utilerias.Documento.CreateTextNode(value));
            return elemento;
        }
        public static DateTime ToDate(DateTime fecha, DateTime hora) => new DateTime(fecha.Year, fecha.Month, fecha.Day, hora.Hour, hora.Minute, 0);
        public static DateTime ToDate(string fecha, string hora) => new DateTime(ToInt(fecha.Substring(0, 4)), ToInt(fecha.Substring(4, 2)), ToInt(fecha.Substring(6, 2)), ToInt(hora.Substring(0, 2)), ToInt(hora.Substring(3, 2)), 0);

        public static DateTime ToDate(string fecha) => new DateTime(ToInt(fecha.Substring(0, 4)), ToInt(fecha.Substring(4, 2)), ToInt(fecha.Substring(6, 2)));
        public static int ToInt(string valor) => Convert.ToInt32(valor);
    }
}
