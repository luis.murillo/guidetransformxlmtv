﻿using com.eninetworks.utilerias.comun;
using com.eninetworks.utilerias.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class SharedString
    {
        public string Programa { get; }
        public string Detalle { get; }
        public static string GetValorByRowStringShare(Control control, int indice, TipoRow row)
        {
            if (row.Items[indice].GetString)
                return control.SharedStrings.Items[Convert.ToInt32(row.Items[indice].Value)].Programa;
            else
                return row.Items[indice].Value.ToString();
        }
        public SharedString(TipoRow row,Control control)
        {
            Programa = GetValorByRowStringShare(control,control.Limites.Fechas.GetPrograma(),row);
            Detalle = GetValorByRowStringShare(control, control.Limites.Fechas.GetDetalle(), row);
        }
        public SharedString(Row row, TipoProcesarArchivo tipoProcesar)
        {
            if (tipoProcesar.CompareTo(TipoProcesarArchivo.SPI) == 0)
            {
                Programa = row.Cells[5].ToString();
                Detalle = row.Cells[6].ToString();
            }
            else if (tipoProcesar.CompareTo(TipoProcesarArchivo.TELEVISA_DEPELICULA) == 0)
            {
                Programa = row.Cells[6].ToString();
                Detalle = row.Cells[5].ToString();
            }
        }
       
        public SharedString(XmlElement item)
        {
            if(item.FirstChild.Name.ToUpper() == "T")
            {
                Programa = item.FirstChild.InnerXml.Trim();
                Detalle = "";
            }
            else
            {
                Programa = GetT(item.ChildNodes[0]);
                Detalle = GetT(item.ChildNodes[1]);
            }
        }
        private string GetT(XmlNode node)
        {
            string resultado = "";
            foreach (XmlNode item in node.ChildNodes)
                if (item.Name.ToUpper() == "T")
                    resultado = item.InnerXml.Trim();
            return resultado;
        }

        public XmlElement GetNodo()
        {
            XmlElement programacion = Utilerias.Documento.CreateElement(string.Empty, "Programacion", string.Empty);
            programacion.AppendChild(Utilerias.SetTextNode("Principal", Programa));

            if (Detalle.Length > 0)
                programacion.AppendChild(Utilerias.SetTextNode("Detalle", Detalle));
            return programacion;
        }

    }
}
