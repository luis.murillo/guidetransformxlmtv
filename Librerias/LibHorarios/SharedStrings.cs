﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class SharedStrings:XML
    {
        public List<SharedString> Items { get; }
        public SharedStrings(FileHorario filehorario)
        {
            Items = new List<SharedString>();
            Load(filehorario.File);
            XmlNode root = XMLDocumento.DocumentElement;
            foreach (XmlElement XmlElement in root.ChildNodes)
                Items.Add(new SharedString(XmlElement));
        }
        public SharedString GetItem(int indice) => Items[indice];
        public SharedString GetItem(double indice) => Items[Convert.ToInt32(indice)];
        public int GetItemToInt(int indice) => Convert.ToInt32(Items[indice].Programa);
        public int GetItemToIntHora(int indice) => Convert.ToInt32(Items[indice].Programa.Split(':')[0]);
    }
}
