﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class Drawing : XML
    {
        public string Semana { get; }
        public int Mes1 { get; }
        public int Mes2 { get; }
        public int Year1 { get; }
        public int Year2 { get; }
        public string Leyenda { get; }
        public string FechaInicio { get; }
        public DateTime DateInicio { get; }
        public DateTime DateFin { get; }
        public string FechaFin { get; }
        public Drawing(FileHorario filehorario, List<XmlNode> nodos, Control control)
        {
            Leyenda = "";
            Semana = "";
            if (!(filehorario is null))
            {
                Load(filehorario.File);
                var nsmgr = new XmlNamespaceManager(XMLDocumento.NameTable);
                nsmgr.AddNamespace("a", "http://schemas.openxmlformats.org/drawingml/2006/main");
                nsmgr.AddNamespace("xdr", "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing");
                foreach (XmlElement item in XMLDocumento.SelectNodes("xdr:wsDr/xdr:twoCellAnchor/xdr:sp/xdr:txBody/a:p/a:r/a:t", nsmgr))
                {
                    if (item.InnerText.ToUpper().Contains("SEMANA"))
                        Semana = item.InnerText;
                    else
                        Leyenda = item.InnerText;
                }
                if (Leyenda.Length>0)
                {
                    if (Leyenda.Split(' ').Length == 7)
                    {
                        FechaInicio = Leyenda.Split(' ')[1] + '/' + Leyenda.Split(' ')[4] + '/' + Leyenda.Split(' ')[6];
                        FechaFin = Leyenda.Split(' ')[3] + '/' + Leyenda.Split(' ')[4] + '/' + Leyenda.Split(' ')[6];
                    }
                    else
                    {
                        FechaInicio = Leyenda.Split(' ')[1] + '/' + Leyenda.Split(' ')[2] + '/' + Leyenda.Split(' ')[4];
                        FechaFin = Leyenda.Split(' ')[6] + '/' + Leyenda.Split(' ')[7] + '/' + Leyenda.Split(' ')[8];
                    }
                    DateInicio = GetDate(FechaInicio);
                    DateFin = GetDate(FechaFin);
                }
                else
                {
                    //Validar cuando trae dos años
                    TipoRow row = new TipoRow(nodos[control.Limites.GetRenglonRangoFecha()]);
                    Leyenda = control.SharedStrings.GetItem(row.Items[control.Limites.GetColumnaRangoFecha()].Value).Programa.Replace(".", "");
                    Semana = control.SharedStrings.GetItem(row.Items[control.Limites.GetColumnaRangoFecha()].Value).Detalle;
                    FechaInicio = Leyenda.Split(' ')[2] + '/' + Leyenda.Split(' ')[4] + '/' + Leyenda.Split(' ')[10];
                    FechaFin = Leyenda.Split(' ')[6] + '/' + Leyenda.Split(' ')[8] + '/' + Leyenda.Split(' ')[10];
                    DateInicio = GetDate(FechaInicio);
                    DateFin = GetDate(FechaFin);
                }
            }
        }
        private DateTime GetDate(string fecha) => new DateTime(Convert.ToInt32(fecha.Split('/')[2]), GetDayMonth(fecha.Split('/')[1]) ,Convert.ToInt32(fecha.Split('/')[0]),0,0,0);
        private int GetDayMonth(string mes)
        {
            if (mes.ToUpper().Contains("ENERO"))
                return 1;
            if (mes.ToUpper().Contains("FEBRERO"))
                return 2;
            if (mes.ToUpper().Contains("MARZO"))
                return 3;
            if (mes.ToUpper().Contains("ABRIL"))
                return 4;
            if (mes.ToUpper().Contains("MAYO"))
                return 5;
            if (mes.ToUpper().Contains("JUNIO"))
                return 6;
            if (mes.ToUpper().Contains("JULIO"))
                return 7;
            if (mes.ToUpper().Contains("AGOSTO"))
                return 8;
            if (mes.ToUpper().Contains("SEPTIEMBRE"))
                return 9;
            if (mes.ToUpper().Contains("OCTUBRE"))
                return 10;
            if (mes.ToUpper().Contains("NOVIEMBRE"))
                return 11;
            if (mes.ToUpper().Contains("DICIEMBRE"))
                return 12;
            else
                return 0;

        }
        public XmlElement GetNodo()
        {
            XmlElement fecha = Utilerias.Documento.CreateElement(string.Empty, "Fecha", string.Empty);
            fecha.AppendChild(Utilerias.SetTextNode("Semana", Semana.ToString()));
            fecha.AppendChild(Utilerias.SetTextNode("Leyenda", Leyenda));
            fecha.AppendChild(Utilerias.SetTextNode("FechaInicio", FechaInicio));
            fecha.AppendChild(Utilerias.SetTextNode("FechaFin", FechaFin));
            return fecha;
        }
    }
}
