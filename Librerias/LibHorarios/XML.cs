﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    abstract class XML
    {
        protected XmlDocument XMLDocumento { get; set; }
        public XML()
        {
            XMLDocumento = new XmlDocument();
        }
        protected void LoadByString(string cadena)
        {
            XMLDocumento.LoadXml(cadena);
        }
        protected void Load(string file)
        {
            XMLDocumento.Load(file);
        }
        protected void Load(XmlReader file)
        {
            XMLDocumento.Load(file);
        }
        protected void Load(TextReader file)
        {
            XMLDocumento.Load(file);
        }
        protected void Load(Stream file)
        {
            XMLDocumento.Load(file);
        }
    }
}
