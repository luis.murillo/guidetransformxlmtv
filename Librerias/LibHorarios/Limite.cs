﻿using com.eninetworks.utilerias.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eninetworks.enitv.horarios
{
    class Limite
    {
        public int DuracionRow { get; }
        public bool TieneDias { get; }
        public string Letra { get; }
        public string Programas{ get; }
        int InicioCelda { get; }
        int FinCelda { get; }
        int InicioRenglon { get; }
        int RenglonDia { get; }
        int RenglonNumeroDia { get; }
        int RenglonRangoFecha { get; }
        int ColumnaRangoFecha { get; }
        public ProgramasIndices ProgramasIndices { get; }
        public ParInicioFin Fechas { get; }
        public Limite(TipoProcesarArchivo tipoProcesarArchivo)
        {
            Fechas = new ParInicioFin(1,1,new ParFecha(0,0),new ParFecha(0,0),1,1,0);
            ProgramasIndices = new ProgramasIndices(TipoArchivo.TIPO1, new Param1[] {
                    new Param1(6,7,2,3,3,4,5,5,0)
                });
            TieneDias = true;
            InicioCelda = 1;
            FinCelda = 1;
            InicioRenglon = 1;
            RenglonNumeroDia = 1;
            RenglonDia = 1;
            Letra = "B";
            RenglonRangoFecha = 4;
            ColumnaRangoFecha = 1;
            Programas = "BCDEFGH";
            DuracionRow = 15;
            if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.SPI) == 0)
            {
                InicioRenglon = 2;
                RenglonDia = 1;
                RenglonNumeroDia = 1;
                InicioCelda = 1;
                TieneDias = false;
                //PROGRAMA-DETALLE-FECHAINICIO-HORAINICIO-MINUTOINICIO-FECHAFIN-HORAFIN-MINUTOFIN-DURACION
                Fechas = new ParInicioFin(6, 7, new ParFecha(2, 3), new ParFecha(4, 5),0,0,0);
                ProgramasIndices = new ProgramasIndices(new string[] { "6-7-2-3-4-5-5-5-0" }, TipoArchivo.TIPO1);
            }
            else if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.TELEVISA_DEPELICULA) == 0)
            {
                InicioRenglon = 4;
                RenglonDia = 1;
                RenglonNumeroDia = 1;
                InicioCelda = 1;
                TieneDias = false;
                Fechas = new ParInicioFin(7, 6, new ParFecha(2, 3),17);
                ProgramasIndices = new ProgramasIndices(new string[] { "7-6-2-3-3-2-3-3-17"}, TipoArchivo.TIPO1);
            }
            else if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.TVAZTECA_CLICK) == 0)
            {
                InicioCelda = 3;
                InicioRenglon = 10;
                RenglonDia = 7;
                RenglonNumeroDia = 8;
                Fechas = new ParInicioFin(1, 1, new ParFecha(0, 0), new ParFecha(0, 0), 1, 2,15);
                Programas = "CDEFGHI";
                ProgramasIndices = new ProgramasIndices(new string[] {
                    "3-3-0-1-2-0-0-0-0"
                    ,"4-4-0-1-2-0-0-0-0"
                    ,"5-5-0-1-2-0-0-0-0"
                    ,"6-6-0-1-2-0-0-0-0"
                    ,"7-7-0-1-2-0-0-0-0"
                    ,"8-8-0-1-2-0-0-0-0"
                    ,"9-8-0-1-2-0-0-0-0"
                    }
                    , TipoArchivo.TIPO2);
            }
            else if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.TELEVISA_DISTRITOCOMEDIA) == 0)
            {
                InicioCelda = 1;
                InicioRenglon = 8;
                RenglonDia = 7;
                RenglonNumeroDia = 7;
                Programas = "BCDEFGH";
                ProgramasIndices = new ProgramasIndices(new string[] {
                    "2-2-0-1-1-0-0-0-0"
                    ,"3-3-0-1-1-0-0-0-0"
                    ,"4-4-0-1-1-0-0-0-0"
                    ,"5-5-0-1-1-0-0-0-0"
                    ,"6-6-0-1-1-0-0-0-0"
                    ,"7-7-0-1-1-0-0-0-0"
                    ,"8-8-0-1-1-0-0-0-0"
                    }
                    , TipoArchivo.TIPO2);
            }
            else if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.TELEVISA_EDGE) == 0)
            {
                InicioCelda = 1;
                InicioRenglon = 8;
                RenglonDia = 7;
                RenglonNumeroDia = 7;
                Programas = "GJMPSVY";
                ProgramasIndices = new ProgramasIndices(new string[] {
                    "6-6-0-1-2-0-0-0-9"
                    }
                    , TipoArchivo.TIPO3);
            }
            else if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.TELEVISA_ASN) == 0)
            {
                InicioCelda = 1;
                InicioRenglon = 8;
                RenglonDia = 7;
                RenglonNumeroDia = 7;
                Programas = "GJMPSVY";
                ProgramasIndices = new ProgramasIndices(new string[] {
                    "7-7-0-1-8-0-0-0-0"
                    ,"9-9-0-1-10-0-0-0-0"
                    ,"11-11-0-1-12-0-0-0-0"
                    ,"13-13-0-1-14-0-0-0-0"
                    ,"15-15-0-1-16-0-0-0-0"
                    ,"17-17-0-1-18-0-0-0-0"
                    ,"19-19-0-1-20-0-0-0-0"
                    }
                    , TipoArchivo.TIPO2);
            }
            else if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.TELEVISA_GOLDEN) == 0)
            {
                InicioCelda = 1;
                InicioRenglon = 8;
                RenglonDia = 7;
                RenglonNumeroDia = 7;
                Programas = "GJMPSVY";
                ProgramasIndices = new ProgramasIndices(new string[] {
                    "7-7-0-1-8-0-0-0-0"
                    ,"9-9-0-1-10-0-0-0-0"
                    ,"11-11-0-1-12-0-0-0-0"
                    ,"13-13-0-1-14-0-0-0-0"
                    ,"15-15-0-1-16-0-0-0-0"
                    ,"17-17-0-1-18-0-0-0-0"
                    ,"19-19-0-1-20-0-0-0-0"
                    }
                    , TipoArchivo.TIPO3);
            }
            else if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.AGROTENDENCIA) == 0)
            {
                InicioCelda = 1;
                InicioRenglon = 8;
                RenglonDia = 7;
                RenglonNumeroDia = 7;
                Programas = "GJMPSVY";
                ProgramasIndices = new ProgramasIndices(new string[] {
                    "7-7-0-1-8-0-0-0-0"
                    ,"9-9-0-1-10-0-0-0-0"
                    ,"11-11-0-1-12-0-0-0-0"
                    ,"13-13-0-1-14-0-0-0-0"
                    ,"15-15-0-1-16-0-0-0-0"
                    ,"17-17-0-1-18-0-0-0-0"
                    ,"19-19-0-1-20-0-0-0-0"
                    }
                    , TipoArchivo.TIPO4);
            }
            else if (tipoProcesarArchivo.CompareTo(TipoProcesarArchivo.TELEVISA_BITME) == 0)
            {
                Fechas = new ParInicioFin(1, 1, new ParFecha(0, 0), new ParFecha(0, 0), 1, 1, 0);
                InicioCelda = 2;
                InicioRenglon = 5;
                RenglonDia = 3;
                RenglonNumeroDia = 3;
                RenglonRangoFecha = 2;
                ColumnaRangoFecha = 1;
                Programas = "BCDEFGH";
                DuracionRow = 30;
                ProgramasIndices = new ProgramasIndices(new string[] {
                    "2-2-0-1-1-0-0-0-0"
                    ,"3-3-0-1-1-0-0-0-0"
                    ,"4-4-0-1-1-0-0-0-0"
                    ,"5-5-0-1-1-0-0-0-0"
                    ,"6-6-0-1-1-0-0-0-0"
                    ,"7-7-0-1-1-0-0-0-0"
                    ,"8-8-0-1-1-0-0-0-0"
                    }
                    , TipoArchivo.TIPO2);
            }

            FinCelda = InicioCelda + 7;
        }
        public int GetInicio() => InicioRenglon - 1;
        public int GetInicioCelda() => InicioCelda - 1;
        public int GetFinCelda() => FinCelda - 1;
        public int GetDia() => RenglonDia - 1;
        public int GetNumeroDia() => RenglonNumeroDia - 1;
        public int GetRenglonRangoFecha() => RenglonRangoFecha - 1;
        public int GetColumnaRangoFecha() => ColumnaRangoFecha - 1;
        public DateTime GetFechaInicio(Control control, TipoRow row) => Utilerias.ToDate(GetFecha(SharedString.GetValorByRowStringShare(control, Fechas.Inicio.GetFecha(), row))
                , GetHora(SharedString.GetValorByRowStringShare(control, Fechas.Inicio.GetHora(), row)));
        public int GetHoraInicio(Control control, TipoRow row) => Convert.ToInt32(GetHora(SharedString.GetValorByRowStringShare(control, Fechas.Inicio.GetHora(), row)).Split(':')[0]);
        public int GetMinutoInicio(Control control, TipoRow row) => Convert.ToInt32(GetHora(SharedString.GetValorByRowStringShare(control, Fechas.Inicio.GetHora(), row)).Split(':')[1]);
        public string GetFecha(string dato)
        {
            string resultado;
            if (dato.Split('/').Length > 1)
                resultado = dato.Split('/')[2] + dato.Split('/')[1] + dato.Split('/')[0];
            else if (dato.Split('-').Length > 1)
                resultado = dato.Split('-')[2] + dato.Split('-')[1] + dato.Split('-')[0];
            else if (dato.Length == 8)
                resultado = dato;
            else
                resultado = DateTime.FromOADate(double.Parse(dato)).ToString("yyyyMMdd");
            return resultado;
        }
        public string GetHora(string dato)
        {
            string resultado;
            if (dato.Split('/').Length > 1)
                resultado = DateTime.ParseExact(dato, "yyyyMMdd HH:mm", null).ToString("HH:mm");
            else if (dato.Split(':').Length > 1)
                resultado = dato;
            else
                resultado = DateTime.FromOADate(Convert.ToDouble(dato)).ToString("HH:mm");
            return resultado;
        }
        public DateTime GetFechaFin(Control control, TipoRow row)
        {
            if (Fechas.ConDuracion)
            {
                DateTime fecha = GetFechaInicio(control, row);
                string dato = GetHora(SharedString.GetValorByRowStringShare(control, Fechas.GetDuracion(), row));
                return fecha.AddHours(Convert.ToDouble(dato.Split(':')[0])).AddMinutes(Convert.ToDouble(dato.Split(':')[1]));
            }
            else
                return Utilerias.ToDate(GetFecha(SharedString.GetValorByRowStringShare(control, Fechas.Fin.GetFecha(), row))
                , GetHora(SharedString.GetValorByRowStringShare(control, Fechas.Fin.GetHora(), row)));
        }
    }
    public class Param1
    {
        public int Programa { get; }
        public int Detalle { get; }
        public int FechaInicio { get; }
        public int FechaFin { get; }
        public int HoraInicio { get; }
        public int HoraFin { get; }
        public int Hora { get; }
        public int Minuto { get; }
        public int Duracion { get; }
        public Param1(int programa, int detalle, int fechainicio, int horainicio, int fechafin, int horafin,
            int hora, int minuto, int duracion)
        {
        Programa  = programa;
        Detalle  = detalle;
        FechaInicio  = fechainicio;
        FechaFin  = fechafin;
        HoraInicio  = horainicio;
        HoraFin  = horafin;
        Hora  = hora;
        Minuto  = minuto;
        Duracion  = duracion;
    }
    }
    public class ProgramasIndices
    {
        public TipoArchivo TipoArchivo{get;}
        public List<ProgramaIndice> Items { get; }
        public ProgramasIndices(string[] valor , TipoArchivo tipoArchivo)
        {
            TipoArchivo = tipoArchivo;
            Items = new List<ProgramaIndice>();
            foreach (string item in valor)
                Items.Add(new ProgramaIndice(item));
        }
        public ProgramasIndices(TipoArchivo tipoArchivo, Param1[] param1 )
        {
            TipoArchivo = tipoArchivo;
            Items = new List<ProgramaIndice>();
            foreach (Param1 item in param1)
                Items.Add(new ProgramaIndice(item));
        }
    }
    public class ProgramaIndice
    {
        public string Letra { get; }
        public int Programa { get; }
        public int Detalle { get; }
        public int Duracion { get; }
        public ParFecha Inicio { get; }
        public ParFecha Fin { get; }
        public ProgramaIndice(string valor)
        {//PROGRAMA-DETALLE-FECHAINICIO-HORAINICIO-MINUTOINICIO-FECHAFIN-HORAFIN-MINUTOFIN-DURACION
            Letra = valor.Split('-')[0];
            Programa = Convert.ToInt32(valor.Split('-')[1]);
            Detalle = Convert.ToInt32(valor.Split('-')[2]);
            Duracion = Convert.ToInt32(valor.Split('-')[8]);
            Inicio = new ParFecha(valor.Split('-'),2) ;
            Fin = new ParFecha(valor.Split('-'), 5);
        }
        public ProgramaIndice(Param1 valor)
        {
            Programa = valor.Programa;
            Detalle = valor.Detalle;
            Duracion = valor.Duracion;
            Inicio = new ParFecha(valor.FechaInicio, valor.HoraInicio);
            Fin = new ParFecha(valor.FechaFin, valor.HoraFin);
        }
    }
    public class ParInicioFin
    {
        int Programa { get; }
        int Detalle { get; }
        int Duracion { get; }
        int Hora { get; }
        int Minuto { get; }
        public int MinutosRestar { get; }
        public bool ConDuracion { get; }
        public ParFecha Inicio { get; }
        public ParFecha Fin { get; }
        public ParInicioFin(int programa, int detalle,ParFecha inicio, ParFecha fin,int hora,int minuto,int minutosrestar)
        {
            Programa = programa;
            Detalle = detalle;
            Inicio = inicio;
            Fin = fin;
            Hora = hora;
            Minuto = minuto;
            MinutosRestar = minutosrestar;
            ConDuracion = false;
        }
        public ParInicioFin(int programa, int detalle, ParFecha inicio, int duracion)
        {
            Programa = programa;
            Detalle = detalle;
            Inicio = inicio;
            Duracion = duracion;
            ConDuracion = true;
        }
        public int GetPrograma() => Programa - 1;
        public int GetDetalle() => Detalle - 1;
        public int GetDuracion() => Duracion - 1;
        public int GetHora() => Hora - 1;
        public int GetMinuto() => Minuto - 1;
    }
    public class ParFecha
    {
        private int Fecha { get; }
        private int Hora { get; }
        private int Minuto { get; }
        public ParFecha(string[] valor,int inicio)
        {
            Fecha = Convert.ToInt32(valor[inicio]);
            Hora = Convert.ToInt32(valor[inicio + 1]); ;
            Minuto = Convert.ToInt32(valor[inicio + 2]); ;
        }
        public ParFecha(int fecha, int hora)
        {
            Fecha = fecha;
            Hora = hora;
            Minuto = 0;
        }
        public int GetFecha() => Fecha - 1;
        public int GetHora() => Hora - 1;
        public int GetMinuto() => Minuto - 1;
    }
}
