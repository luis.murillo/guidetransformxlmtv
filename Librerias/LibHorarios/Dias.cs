﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class Dias
    {
        public List<DiaSemana> Items { get; }
        public Dias(){
            Items = new List<DiaSemana>();
        }
        public XmlElement GetNodo()
        {
            XmlElement dias = Utilerias.Documento.CreateElement(string.Empty, "Dias", string.Empty);
            foreach (DiaSemana item in Items)
                dias.AppendChild(item.GetNodo());
            return dias;
        }
    }
}
