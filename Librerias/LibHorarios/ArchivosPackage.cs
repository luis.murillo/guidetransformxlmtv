﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using com.eninetworks.utilerias.enums;
using com.eninetworks.utilerias.excel;

namespace com.eninetworks.enitv.horarios
{

    class ArchivosPackage
    {
        string File { get; }
        public Control Control { get; }
        public string Canal { get; }
        public string IdCanal { get; }
        List<Programa> Programas { get; }
        public ArchivosPackage(string file, string canal, TipoProcesarArchivo tipoProcesarArchivo)
        {
            Canal = canal;
            IdCanal = canal + Propiedades.GetStringPropertie("IDCANALBASE");
            File = file;
            Programas = new List<Programa>();
            Control = new Control(Programas, File, tipoProcesarArchivo);
        }
        public void SaveXML()
        {
            GetXmlDocumento().Save(File.ToUpper().Replace("XLSX", "xml").Replace("XLS", "xml"));
        }
        public void SaveXMLTV()
        {
            GetXmlDocumentoTV().Save(File.ToUpper().Replace("XLSX", "xml").Replace("XLS", "xml"));
        }
        private XmlDocument GetXmlDocumentoTV()
        {
            Utilerias.CreateXMLDocumentTV();
            XmlElement canal = Utilerias.Documento.CreateElement(string.Empty, "channel", string.Empty);
            canal.SetAttribute("id", IdCanal);
            XmlElement display = Utilerias.Documento.CreateElement(string.Empty, "display-name", string.Empty);
            display.InnerText = Canal;
            canal.AppendChild(display);
            Utilerias.Root.AppendChild(canal);
            foreach (Programa item in Programas)
                if (Control.IsTypeOne())
                    item.SetNodoTV2(Utilerias.Root, IdCanal);
                else
                    item.SetNodoTV(Utilerias.Root, IdCanal);
            return Utilerias.Documento;
        }
        private XmlDocument GetXmlDocumento()
        {
            Utilerias.CreateXMLDocument();
            Utilerias.Root.AppendChild(Control.Drawing.GetNodo());
            XmlElement programas = Utilerias.Documento.CreateElement(string.Empty, "Programas", string.Empty);
            Utilerias.Root.AppendChild(programas);
            foreach (Programa item in Programas)
                item.SetNodo(programas);
            return Utilerias.Documento;
        }
    }
}
