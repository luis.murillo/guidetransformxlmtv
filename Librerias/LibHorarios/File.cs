﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eninetworks.enitv.horarios
{
    class FileHorario
    {
        public Stream File { get; }
        public string FileName { get; }
        public FileHorario(Stream file,string filename)
        {
            File = file;
            FileName = filename.ToUpper();
        }

    }
}
