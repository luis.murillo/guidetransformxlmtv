﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eninetworks.enitv.horarios
{
    static class Propiedades
    {
        public static string GetConfig()
        {
            StringBuilder resultado = new StringBuilder();
            try
            {
                var section = ConfigurationManager.GetSection("applicationSettings");
                NameValueCollection appSettings = ConfigurationManager.AppSettings;
                for (int i = 0; i < appSettings.Count; i++)
                    resultado.Append(String.Format("#{0} Key: {1} Value: {2}", i + 1, appSettings.GetKey(i), appSettings[i]) + Environment.NewLine);
            }
            catch (Exception)
            {

            }
            return resultado.ToString();
        }
        public static string Add(string name, string value)
        {
            string resultado;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings.Add(name, value);
                config.Save(ConfigurationSaveMode.Minimal);
                resultado = "OK";
            }
            catch (Exception ex)
            {
                resultado = "ERROR:" + ex.Message;
            }
            return resultado;
        }
        public static string Update(string name, string value)
        {
            string resultado;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings[name].Value = value;
                config.Save(ConfigurationSaveMode.Minimal);
                resultado = "OK";
            }
            catch (Exception ex)
            {
                resultado = "ERROR:" + ex.Message;
            }
            return resultado;
        }
        public static string GetStringPropertie(string name) => ConfigurationManager.AppSettings[name].ToString().Trim();
        public static int GetIntPropertie(string name) => Convert.ToInt32(ConfigurationManager.AppSettings[name].ToString().Trim());
        public static bool GetBoolPropertie(string name)
        {
            if (ConfigurationManager.AppSettings[name].ToString().Trim().ToUpper() == "TRUE")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool IsNumeric(string loValor)
        {
            bool lbResultado = false;

            try
            {
                Convert.ToDecimal(loValor);

                lbResultado = true;
            }
            catch (Exception)
            {
            }

            return lbResultado;
        }
    }
}
