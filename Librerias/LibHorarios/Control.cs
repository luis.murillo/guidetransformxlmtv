﻿using com.eninetworks.utilerias.enums;
using com.eninetworks.utilerias.excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class Control
    {
        public Drawing Drawing { get; set; }
        private int LastHora { get; set; }
        public TipoProcesarArchivo TipoProcesarArchivo { get; }
        public SharedStrings SharedStrings { get; }
        public Limite Limites { get; }
        public Read Read { get; }
        public SheetData Sheet { get; }
        public Control(List<Programa> Programas, string file, TipoProcesarArchivo tipoProcesarArchivo)
        {
            TipoProcesarArchivo = tipoProcesarArchivo;
            Limites = new Limite(TipoProcesarArchivo);
            Read = new Read(file, tipoProcesarArchivo, 0);
            foreach (string hoja in Read.Hojas)
            {
                LastHora = 0;
                Read.Load(hoja);
                SharedStrings = new SharedStrings(Read.Archivos.GetFileByName("SHAREDSTRINGS"));
                Sheet = new SheetData(Read.Archivos.GetFileByName("SHEET"), this);
                if (IsTypeOne())
                    foreach (TipoRow row in Sheet.Items)
                        Programas.Add(new Programa(row, this));
                if (IsTypeTwo())
                    foreach (TipoRow row in Sheet.Items)
                        foreach (TipoCel cel in row.Items.FindAll(e => e.IsPrograma))
                            Programas.Add(new Programa(cel, this, GetHora(row), GetMinuto(row)));
                else
                    foreach (TipoRow row in Sheet.Items)
                        foreach (TipoCel cel in row.Items.FindAll(e => e.IsPrograma))
                            Programas.Add(new Programa(cel, this, GetHora(row), GetMinuto(row)));
            }
        }
        private int GetHora(TipoRow row)
        {
            int resultado=0;

            try
            {
                TipoCel cel = row.Items[Limites.Fechas.GetHora()];

                if (cel.GetString)
                {
                    string[] valor = SharedStrings.GetItem(cel.Value).Programa.Split(':');
                    if (valor[0].Length == 0)
                        resultado = LastHora;
                    else
                        resultado = Convert.ToInt32(valor[0]);
                }
                else
                {
                    if (cel.Value.ToString().Contains('.'))
                    {
                        resultado = Convert.ToInt32(Limites.GetHora(cel.Value.ToString()).Split(':')[0]);
                    }
                    else
                    {
                        resultado = Convert.ToInt32(cel.Value);
                    }

                }
                LastHora = resultado;

            }
            catch (Exception ex)
            {

            }
            
            return resultado;
        }
        private int GetMinuto(TipoRow row)
        {
            TipoCel cel = row.Items[Limites.Fechas.GetMinuto()];
            int resultado = Convert.ToInt32(cel.Value);
            if (cel.GetString)
            {
                string[] valor = SharedStrings.GetItem(resultado).Programa.Split(':');
                resultado = 0;
                if (valor.Length == 2)
                    resultado = Convert.ToInt32(valor[1]);
            }
            resultado -= Limites.Fechas.MinutosRestar;
            return resultado;
        }
        public bool IsTypeOne() => new List<TipoProcesarArchivo>(
                new TipoProcesarArchivo[] { TipoProcesarArchivo.SPI, TipoProcesarArchivo.TELEVISA_DEPELICULA })
                .FindAll(e => e.CompareTo(TipoProcesarArchivo) == 0).Count > 0 ? true : false;
        public bool IsTypeTwo() => new List<TipoProcesarArchivo>(
                new TipoProcesarArchivo[] { TipoProcesarArchivo.TVAZTECA_CLICK, TipoProcesarArchivo.TELEVISA_DISTRITOCOMEDIA, TipoProcesarArchivo.TELEVISA_BITME })
                .FindAll(e => e.CompareTo(TipoProcesarArchivo) == 0).Count > 0 ? true : false;

    }
}
