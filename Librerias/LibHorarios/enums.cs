﻿using System;
using System.Collections.Generic;
using System.Text;

namespace com.eninetworks.utilerias.enums
{
    public enum Version{
        Version_1
    }
    public enum Extension
    {
        XLS,
        XLSX,
        CSV,
        NONE
    }
    public enum TipoRow
    {
        Tipo,
        Encabezado,
        Item
    }
    public enum TipoProcesarArchivo
    {
        SPI,
        TELEVISA_DEPELICULA,
        TVAZTECA_CLICK,
        TELEVISA_DISTRITOCOMEDIA,
        TELEVISA_EDGE,
        TELEVISA_ASN,
        TELEVISA_GOLDEN,
        AGROTENDENCIA,
        TELEVISA_BITME
    }
    public enum TipoArchivo
    {
        TIPO1,
        TIPO2,
        TIPO3,
        TIPO4
    }

}
