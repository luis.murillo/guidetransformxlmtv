﻿using System;
using System.Collections.Generic;
using System.Text;

namespace com.eninetworks.utilerias.comun
{
    class Row
    {
        public List<string> Cells { get; set; }
        public Row()
        {
            Cells = new List<string>();
        }
    }
}
