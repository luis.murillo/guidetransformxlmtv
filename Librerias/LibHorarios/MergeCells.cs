﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    static class MergeCells
    {
        static List<MergeCell> Items { get; set; }
        public static void Load(XmlNode node)
        {
            Items = new List<MergeCell>();
            if(!(node is null))
                foreach (XmlNode item in node.ChildNodes)
                    Items.Add(new MergeCell(item));
        }
        public static MergeCell GetMergeCell(string inicio) => Items.Find(e => e.Inicio.Valor == inicio);
        
        public static void Clear()
        {
            Items?.Clear();
        }
    }
}
