﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class DiaSemana
    {
        public string Nombre { get; }
        public string Letra { get; }
        public DateTime Fecha { get; }
        public double Dia { get; set; }
        public int Indice { get; }
        public  DiaSemana(int indice, Control Control,DateTime fecha)
        {
            Nombre = fecha.ToString("dddd").ToUpper();
            Letra = Control.Limites.Letra;
            Dia = fecha.Day;
            Indice = indice;
            Fecha = fecha;
        }
        public DiaSemana(TipoCel dia, TipoCel numero, int indice, Control Control)
        {
            Nombre = Utilerias.GetDiaSemana(dia.Value);
            Letra = Utilerias.GetLetraSemana(dia.Value);
            Dia = numero.Value;
            Indice = indice;
            Fecha = Control.Drawing.DateInicio.AddDays(dia.Value - 1);
        }
        public DiaSemana(TipoCel celda, int indice, Control Control)
        {
            if (celda.GetString)
            {
                Nombre = Control.SharedStrings.Items[Convert.ToInt32(celda.Value)].Programa.Split(' ')[0].ToUpper();                
                Dia = Convert.ToInt32(Control.SharedStrings.Items[Convert.ToInt32(celda.Value)].Programa.Split(' ')[1]);
                Fecha = Control.Drawing.DateInicio.AddDays(Utilerias.GetDiasSemana(Control.Limites.GetInicioCelda(), Letra) - 1);
            }
            else
            {
                CultureInfo ci = new CultureInfo("Es-Es");
                Fecha=Utilerias.ToDate(Control.Limites.GetFecha(celda.Value.ToString()));
                Nombre = ci.DateTimeFormat.GetDayName(Fecha.DayOfWeek).ToString().ToUpper();
                Dia = Fecha.Day;
            }
            Letra = Utilerias.GetLetraCelda(celda.Indice);
            Indice = indice;
        }
        public XmlElement GetNodo()
        {
            XmlElement dia = Utilerias.Documento.CreateElement(string.Empty, "Dia", string.Empty);
            dia.AppendChild(Utilerias.SetTextNode("DiaNumero", Dia.ToString()));
            dia.AppendChild(Utilerias.SetTextNode("DiaSemana", Nombre));
            return dia;
        }
    }
}
