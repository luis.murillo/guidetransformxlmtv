﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eninetworks.enitv.horarios
{
    class Rango
    {
        public string Valor { get; }
        public int Indice { get; }
        public string Letra { get; }
        public Rango(string valor)
        {
            Valor = valor;
            Indice = Convert.ToInt32(valor.Substring(1, valor.Length - 1));
            Letra = Valor.Substring(0, 1);
        }
    }
}
