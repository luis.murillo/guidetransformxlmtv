﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{    class TipoCel
    {
        public int Row { get; }
        public string Indice { get; }
        public int Size { get; }
        public double Value { get; }
        public bool GetString { get; }
        public bool IsPrograma { get; }
        public TipoCel(XmlNode node, int rowid)
        {
            try
            {
                Row = rowid;
                Indice = node.Attributes["r"].Value;
                Size = Convert.ToInt32(Utilerias.GetValor(node, "s"));
                GetString = false;
                if (Utilerias.GetValor(node, "t").ToUpper().Contains("S"))
                    GetString = true;
                if (node.HasChildNodes)
                    Value = double.Parse(node.FirstChild.InnerText, NumberStyles.AllowExponent | NumberStyles.Float, CultureInfo.InvariantCulture);
                IsPrograma = DiasSemanas.IsProgram(Indice, GetString);
            }
            catch (Exception ex)
            {

            }
        }
        public TipoCel(XmlNode node, int rowid, ProgramaIndice programaIndice)
        {
            Row = rowid;
            Indice = node.Attributes["r"].Value;
            Size = Convert.ToInt32(Utilerias.GetValor(node, "s"));
            GetString = false;
            if (Utilerias.GetValor(node, "t").ToUpper().Contains("S"))
                GetString = true;
            if (node.HasChildNodes)
                Value = Convert.ToDouble(node.FirstChild.InnerText);
            IsPrograma = DiasSemanas.IsProgram(Indice, GetString);
        }
    }
}
