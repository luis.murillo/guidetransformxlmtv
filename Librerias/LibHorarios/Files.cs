﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eninetworks.enitv.horarios
{
    class Files
    {
        List<FileHorario> Items { get; }
        private int Selected { get; set; }
        public Files()
        {
            Items = new List<FileHorario>();
            Selected = 0;
        }
        public void Add(Stream file, string filename)
        {
            Items.Add(new FileHorario(file, filename));
            Selected = Items.Count -1;
        }
        public void Clear()
        {
            Items.Clear();
        }
        public List<FileHorario> GetItems() => Items;
        public FileHorario GetFileByName(string name) => Items.Find(e => e.FileName.Contains(name));
        public FileHorario GetItem() => Items[Selected];
    }
}
