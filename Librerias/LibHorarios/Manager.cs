﻿using com.eninetworks.utilerias.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.eninetworks.enitv.horarios
{
    public class Manager
    {
        ArchivosPackage ArchivosPackage { get; }
        List<TipoProcesarArchivo> PermitirArchivosPackage { get; }
        TipoProcesarArchivo TipoProcesarArchivo { get; }
        public Manager(string file,string canal, TipoProcesarArchivo tipoProcesarArchivo)
        {
            PermitirArchivosPackage = new List<TipoProcesarArchivo>( new TipoProcesarArchivo[] {
                TipoProcesarArchivo.TELEVISA_DISTRITOCOMEDIA, TipoProcesarArchivo.TVAZTECA_CLICK } );
            TipoProcesarArchivo = tipoProcesarArchivo;
            ArchivosPackage = new ArchivosPackage(file,canal, tipoProcesarArchivo);
        }
        public bool ValidPermitirArchivosPackage()
        {
            TipoProcesarArchivo tipo = PermitirArchivosPackage.Find(e => e.CompareTo(TipoProcesarArchivo) == 0);
            return tipo.CompareTo(TipoProcesarArchivo)==0?true:false;
        }
        public void SaveXML()
        {
            ArchivosPackage.SaveXML();
        }
        public void SaveXMLTV()
        {
            ArchivosPackage.SaveXMLTV();
        }
    }
}
