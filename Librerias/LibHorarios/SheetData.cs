﻿using com.eninetworks.utilerias.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class SheetData:XML
    {
        public List<TipoRow> Items { get; set; }
        public SheetData(FileHorario filehorario, Control control)
        {
            Load(filehorario.File);
            MergeCells.Load(GetNodoByName("mergeCells"));
            XmlNode node = GetDocumentElement();
            Items = new List<TipoRow>();
            List<XmlNode> Nodos = node.ChildNodes.Cast<XmlNode>().ToList();
            DiasSemanas.Init();
            control.Drawing = new Drawing(control.Read.Archivos.GetFileByName("DRAWING"), Nodos, control);
            DiasSemanas.Inicio = control.Limites.GetInicio();
            if (control.Limites.TieneDias)
                DiasSemanas.Load(new TipoRow(Nodos[control.Limites.GetDia()]), new TipoRow(Nodos[control.Limites.GetNumeroDia()]), control);
            Nodos.RemoveRange(0, control.Limites.GetInicio());
            foreach (XmlNode item in Nodos)
                Items.Add(new TipoRow(item));
        }
        private XmlNode GetNodoByName(string name) => (XmlNode)XMLDocumento.DocumentElement.GetElementsByTagName(name).Item(0);
        public XmlNode GetDocumentElement() => GetNodoByName("sheetData");
    }
}
