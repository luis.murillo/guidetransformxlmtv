﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace com.eninetworks.enitv.horarios
{
    class MergeCell
    {
        public string Merge { get; }
        public Rango Inicio { get; }
        public Rango Fin { get; }
        public int Duracion { get; }
        public MergeCell(XmlNode node)
        {
            Merge = node.Attributes["ref"].Value;
            Inicio = new Rango(Merge.Split(':')[0]);
            Fin = new Rango( Merge.Split(':')[1]);
            Duracion = (Fin.Indice - Inicio.Indice + 1);
        }
    }
}
