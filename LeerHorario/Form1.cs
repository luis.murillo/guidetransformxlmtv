﻿using com.eninetworks.enitv.horarios;
using com.eninetworks.utilerias.enums;
using com.eninetworks.utilerias.excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeerHorario
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                new Manager(txtFILENAME.Text, txtCANAL.Text, (TipoProcesarArchivo) cmbTIPOS.SelectedItem).SaveXMLTV();
                MessageBox.Show("OK");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                }
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtFILENAME.Text = openFileDialog1.FileName;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cmbTIPOS.DataSource = Enum.GetValues(typeof(TipoProcesarArchivo));
            //cmbTIPOS.SelectedIndex = 3;
        }
    }
}
