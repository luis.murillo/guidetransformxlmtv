﻿namespace LeerHorario
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.txtFILENAME = new System.Windows.Forms.TextBox();
            this.txtCANAL = new System.Windows.Forms.TextBox();
            this.cmbTIPOS = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(717, 47);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Process";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(589, 47);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 1;
            this.button2.Text = "Open";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // txtFILENAME
            // 
            this.txtFILENAME.Location = new System.Drawing.Point(61, 47);
            this.txtFILENAME.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFILENAME.Name = "txtFILENAME";
            this.txtFILENAME.Size = new System.Drawing.Size(495, 22);
            this.txtFILENAME.TabIndex = 2;
            this.txtFILENAME.Text = "C:\\Users\\JDIAZC\\Downloads\\DISTRITO COMEDIA ENERO 2020 (19DIC19) (3).xlsx";
            // 
            // txtCANAL
            // 
            this.txtCANAL.Location = new System.Drawing.Point(61, 16);
            this.txtCANAL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCANAL.Name = "txtCANAL";
            this.txtCANAL.Size = new System.Drawing.Size(257, 22);
            this.txtCANAL.TabIndex = 3;
            this.txtCANAL.Text = "CLIC";
            // 
            // cmbTIPOS
            // 
            this.cmbTIPOS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTIPOS.FormattingEnabled = true;
            this.cmbTIPOS.Location = new System.Drawing.Point(61, 90);
            this.cmbTIPOS.Name = "cmbTIPOS";
            this.cmbTIPOS.Size = new System.Drawing.Size(181, 24);
            this.cmbTIPOS.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.cmbTIPOS);
            this.Controls.Add(this.txtCANAL);
            this.Controls.Add(this.txtFILENAME);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtFILENAME;
        private System.Windows.Forms.TextBox txtCANAL;
        private System.Windows.Forms.ComboBox cmbTIPOS;
    }
}

